package com.example.ninat.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

/**
 * Created by ninat on 17/05/2017.
 */

@Table
public class Friend {
    private Long id;
    private Person friend;
    private Person person;

    public Friend(){}

    public Friend(Person person, Person friend) {
        this.friend = friend;
        this.person = person;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getFriend() {
        return friend;
    }

    public void setFriendId(Person friend) {
        this.friend = friend;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


}

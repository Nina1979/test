package com.example.ninat.test.rest;

import com.example.ninat.test.model.Person;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Path;

/**
 * Created by ninat on 14/05/2017.
 */

public interface WebAPI {


    @GET("data.json")
    Call<List<Person>> getData();
}

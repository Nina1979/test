package com.example.ninat.test.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ninat.test.R;
import com.example.ninat.test.activity.DisplayFriends;
import com.example.ninat.test.model.Friend;
import com.example.ninat.test.model.Person;
import com.google.gson.JsonObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.bitmap;

/**
 * Created by ninat on 14/05/2017.
 */

public class ListAdapter extends ArrayAdapter {
    List<Person> mPersons;
    Context mContext;

    public ListAdapter(Context context, List<Person> persons) {
        super(context, 0, persons);
        this.mPersons = persons;
        this.mContext = context;
    }




    //saves accesses to views
    static class ViewHolder {
        @BindView(R.id.txtName)
        TextView txtName;

        @BindView(R.id.imgSign)
        ImageView imgSign;





        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ViewHolder viewHolder;
        final Person item = (Person) getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.txtName.setText(item.getFirstName()+ " " +item.getSurname() );
        if (item.getGender().equals("female")){

            viewHolder.imgSign.setImageResource(R.drawable.female);


        }
        else
            viewHolder.imgSign.setImageResource(R.drawable.male);

        viewHolder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, DisplayFriends.class);
                //Checks if person exists. If it exists it takes its id and puts it into intent message
                if (item != null) {
                    Long mId = item.getId();

                    intent.putExtra("personID", mId);
                    mContext.startActivity(intent);

                }
            }
        });
        // Return the completed view to render on screen
        return convertView;
        };

}
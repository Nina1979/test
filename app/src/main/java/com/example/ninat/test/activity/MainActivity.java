package com.example.ninat.test.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import android.support.annotation.Nullable;

import com.example.ninat.test.R;
import com.example.ninat.test.adapter.ListAdapter;
import com.example.ninat.test.model.Friend;
import com.example.ninat.test.model.Person;
import com.example.ninat.test.rest.ApiClient;
import com.example.ninat.test.rest.WebAPI;
import com.orm.SugarRecord;
import com.orm.query.Select;


import java.util.List;

import butterknife.ButterKnife;
import retrofit2.Call;
import butterknife.BindView;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ListAdapter mAdapter;
    @BindView(R.id.personList)
    ListView lvPersonList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //takes persons from database
        List<Person> mBPersons = Select.from(Person.class).list();


        for(Person friend: mBPersons) {
            List <Person> i =friend.getFriends();

        }


        if (mBPersons.isEmpty()) {
            // Fetch persons from web api
            getUsersFromOnlineAPI();
        } else {
            // We have persons in database
            // Creates an instance of an mAdapter and sets it in listview
            mAdapter = new ListAdapter(MainActivity.this, mBPersons);
            lvPersonList.setAdapter(mAdapter);
        }
//
//        Person p = new Person();
//        this.friendsAdapter = new ListAdapter(this,p.getFriends()); // Direct friends
//        // Friends of friends;
//        ArrayList<Person> friendFriends = new ArrayList<Person>();
//        for(Person friend: p.getFriends()){
//            for(Person friendOfFriend: friend.getFriends()){
//                //!!! Check that it's not your friend
//                //!!! Check that it is not already in list
//                friendFriends.add(friendOfFriend);
//            }
//        }
//        this.friendsOfFriendsAdapter = new ListAdapter(this, friendFriends);
    }

    public void getUsersFromOnlineAPI(){
        try {
            //Sends a call to a server
            WebAPI api = ApiClient.getClient().create(WebAPI.class);
            Call<List<Person>> call = api.getData();
            call.enqueue(new Callback<List<Person>>() {
                @Override
                public void onResponse(Call<List<Person>> call, Response<List<Person>> response) {
                    switch (response.code()) {
                        case 200:
                            List<Person> persons = response.body();
                            //Saves persons to database before linking friends
                            for (Person p : persons) {
                                SugarRecord.save(p);
                            }
                            // Link friends after all persons are saved
                            // to prevent saving friends when friends Person
                            // object is not yet saved to database
                            for (Person p: persons){
                                for (Long friendId: p.getFriendIds()){
                                    Person friend = SugarRecord.findById(Person.class,friendId);
                                    Friend friendRecord = new Friend(p, friend);
                                    SugarRecord.save(friendRecord);
                                }
                            }
                            mAdapter = new ListAdapter(MainActivity.this, persons);
                            lvPersonList.setAdapter(mAdapter);
                        case 401:
                            Toast.makeText(MainActivity.this, "System error!", Toast.LENGTH_LONG).show();
                            break;
                        case 406:
                            Toast.makeText(MainActivity.this, "System error!", Toast.LENGTH_LONG).show();
                            break;
                        case 415:
                            Toast.makeText(MainActivity.this, "System error!", Toast.LENGTH_LONG).show();
                            break;
                    }
                }

                @Override
                public void onFailure(Call<List<Person>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}






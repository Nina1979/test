package com.example.ninat.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;

import org.powermock.core.classloader.annotations.PowerMockIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ninat on 14/05/2017.
 */
@Table
public class Person implements Serializable {
    @Expose
    private Long id;
    @Expose
    private String firstName;
    @Expose
    private String surname;
    @Expose
    private String age;
    @Expose
    private String gender;

    @SerializedName("friends")
    @Expose
    @Ignore // ignore saving to database
    public List<Long> friendIds = new ArrayList<Long>();

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) return false;
//        if (!obj.getClass() == Person.class) return false;
//
//        Person pObj = (Person)obj;
//        if (pObj.getId().equals(this.getId()))
//           return true;
//        return false;
//    }


    public List<Person> getFriends() {

            List<Friend> friends = SugarRecord.find(Friend.class, "person = ?", getId().toString());
            ArrayList<Person> personFriends = new ArrayList<>();
            for (Friend f : friends) {
                personFriends.add(f.getFriend());
            }

            return personFriends;


    }
    public Person() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Long> getFriendIds() {
        return friendIds;
    }

    public void setFriendsIds(List<Long> friends) {
        this.friendIds = friends;
    }



}

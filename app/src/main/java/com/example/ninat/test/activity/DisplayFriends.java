package com.example.ninat.test.activity;

import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.ninat.test.R;
import com.example.ninat.test.adapter.ListAdapter;
import com.example.ninat.test.model.Person;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ninat on 18/05/2017.
 */

public class DisplayFriends extends AppCompatActivity {

    private ListAdapter friendsAdapter;
    private ListAdapter friendsOfFriendsAdapter;
    private ListAdapter suggestedFriendsAdapter;

    @BindView(R.id.FriendsofFriendsList)
    ListView FriendsofFriendsList;


    @BindView(R.id.DirectFriendsList)
    ListView lvDirectFrindsList;

    @BindView(R.id.SuggestedFriendsList)
    ListView SuggestedFriendsList;





    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_friends);
        ButterKnife.bind(this);
        //Takes message from intent by the key
        Bundle extras = getIntent().getExtras();
        Long personID = extras.getLong("personID");

        // Direct friends
        Person person = SugarRecord.findById(Person.class, personID);
        friendsAdapter = new ListAdapter(this, person.getFriends());
        lvDirectFrindsList.setAdapter(friendsAdapter);
        ListUtils.setDynamicHeight(lvDirectFrindsList);


        // Friends of friends;
        ArrayList<Person> friendFriends = new ArrayList<Person>();
        for (Person f : person.getFriends()) {
            for (Person friendOfFriend : f.getFriends()) {
                Long ffId = friendOfFriend.getId();
                Long myId = person.getId();

                //!!! Check if that friend are not we
                if (ffId.equals(myId)) continue;

                // Check that this person is not already added
                Boolean found = false;
                for (Person inList: friendFriends){
                    if (friendOfFriend.getId().equals(inList.getId())){
                        found = true;
                        break;
                    }
                }
                if (found) continue;

                // Check if that person is in list of frinds for selected user
                found = false;
                for (Person myFriend : person.getFriends()) {
                    if (myFriend.getId().equals(ffId)) {
                        found = true;
                        break;
                    }
                }
                if (found) continue;


                friendFriends.add(friendOfFriend);
            }
        }
        friendsOfFriendsAdapter = new ListAdapter(this, friendFriends);
        FriendsofFriendsList.setAdapter(friendsOfFriendsAdapter);
        ListUtils.setDynamicHeight(FriendsofFriendsList);

        //Suggested friends
        ArrayList<Person> suggestedFriends = new ArrayList<Person>();
        List<Person> allPersons = SugarRecord.listAll(Person.class);
        for (Person p : allPersons) {
            // Check it's not us
            if (person.getId().equals(p.getId())) continue;

            int sameFriends = 0;
            for (Person myFriend: person.getFriends()){
                for (Person hisFriend: p.getFriends()){
                    if (myFriend.getId().equals(hisFriend.getId())){
                        sameFriends++;
                        break;
                    }
                }
            }
            if (sameFriends < 2) continue;

            suggestedFriends.add(p);
        }

        suggestedFriendsAdapter = new ListAdapter(this, suggestedFriends);
        SuggestedFriendsList.setAdapter(suggestedFriendsAdapter);
        ListUtils.setDynamicHeight(SuggestedFriendsList);
    }
    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = (ListAdapter) mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

}


